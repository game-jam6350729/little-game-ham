extends ResourceNode
class_name Trough

signal TroughOpen
@onready var sprite_empty : Sprite2D = $TextureEmpty
@onready var sprite_full : Sprite2D = $TextureFull
@export var max_element : int = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	super._ready()
	


func _physics_process(_delta):
	var is_empty = current_resources == 0
	sprite_empty.visible = is_empty
	sprite_full.visible = not is_empty



func refill(amount : int):
	if amount + current_resources >= max_element :
		current_resources = max_element
	else:
		current_resources += amount
