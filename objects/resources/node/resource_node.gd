extends StaticBody2D
class_name ResourceNode



@export var starting_resources: int = 1
@export var is_to_delete : bool = false
@export var node_type : ResourceNodeType
@export var resource_item : ResourceItem
@export var pickup_sfx : AudioStreamPlayer

var is_gatherable : bool = true

var current_resources : int :
	set(value):
		if pickup_sfx and value < current_resources:
			pickup_sfx.play()
		current_resources = value
		#A resource node emptied is removed
		if(value <= 0 and is_to_delete):
			is_gatherable = false
			if not pickup_sfx.playing:
				queue_free()
			else:
				hide()
				pickup_sfx.finished.connect(queue_free)

# Called when the node enters the scene tree for the first time.
func _ready():
	current_resources = starting_resources



func harvest(amount: int) -> bool:
	if current_resources > 0:
		current_resources -= amount
		is_gatherable = true
	else:
		is_gatherable = false
	return is_gatherable
