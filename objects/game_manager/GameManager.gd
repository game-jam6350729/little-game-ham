extends Node
class_name GameManager

@onready var store_menu : StoreMenu = %StoreMenu
@onready var pause_menu : PauseMenu = %PauseMenu
@onready var trough_menu : TroughMenu = %TroughMenu
@onready var timer : Timer = %Timer
@export var gold : ResourceItem

var is_pause_open : bool = false
var is_store_open : bool = false
var is_trough_open : bool = false


signal toto
func _ready():
	
	for pig in get_tree().get_nodes_in_group("Pig"):
		pig.isDead.connect(func(): SceneTransition.change_scene_to_file("res://levels/GameOver.tscn"))
	
	for store in get_tree().get_nodes_in_group("Store"):
		if not store.ShopOpen.is_connected(_on_store_open):
			store.ShopOpen.connect(_on_store_open)
	store_menu.StoreClose.connect(func(): is_store_open =false)
	pause_menu.PauseClose.connect(func(): is_pause_open =false)
	trough_menu.TroughClose.connect(func(): is_trough_open =false)
	
	(get_tree().get_first_node_in_group("Player").get_node("Inventory") as Inventory ).add_resources(gold, 0)
	
	for trough in get_tree().get_nodes_in_group("Trough"):
		trough.TroughOpen.connect(func(): _on_trough_open(trough))

func _notification(notification):
	match notification:
		NOTIFICATION_APPLICATION_FOCUS_OUT:
			if name != "Debug":
				pause_menu.pause()
				is_pause_open = true
		

func _process(_delta):
	get_tree().paused = is_store_open or is_pause_open or is_trough_open

func _unhandled_input(event: InputEvent)-> void:
	if event.is_action_pressed("ingame_pause"):
		is_pause_open = true
	if event.is_action_pressed("test"):
		_on_store_open()

func _on_store_open():
	is_store_open = true
	store_menu.open()
	
func _on_trough_open(trough):
	trough_menu.open(trough) 
	is_trough_open= true
