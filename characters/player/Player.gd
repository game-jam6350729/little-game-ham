extends CharacterBody2D
class_name Player 

@export var speed : float = 5

# Called when the node enters the scene tree for the first time.
func _ready()-> void:
	pass # Replace with function body.


	
func _physics_process(delta)-> void:
	var axis = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down").normalized()
	velocity = axis * (speed * 1_000) * delta
	move_and_slide()

	
