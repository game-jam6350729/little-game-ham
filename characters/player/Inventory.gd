extends Node
class_name Inventory

@export var resources : Dictionary = {}
signal resource_count_changed(type: ResourceItem, new_count: int)



# Add a resource to the player inventory
func add_resources( type : Resource, amount : int) -> void:
	if amount < 0:
		return
	if (!resources.has(type)):
		resources[type] = 0
	resources[type] += amount
	resource_count_changed.emit(type, resources[type])

func remove_resources( type : Resource, amount : int) -> bool:
	if (resources.has(type) and resources[type] > 0):
		resources[type] -= amount
		resource_count_changed.emit(type, resources[type])
		return true
	return false
