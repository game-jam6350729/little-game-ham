using Godot;

namespace littlegameham.entity.player;

public partial class Player : CharacterBody2D
{

	[Export] public float _speed  = 10;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

	public override void _PhysicsProcess(double delta)
	{
		var inputVector = Vector2.Zero;
		inputVector.X = Input.GetActionStrength("ui_right") - Input.GetActionStrength("ui_left");
		inputVector.Y = Input.GetActionStrength("ui_down") - Input.GetActionStrength("ui_up");
	
		Velocity = inputVector != Vector2.Zero ? inputVector.Normalized() : Vector2.Zero;

		MoveAndCollide(Velocity * _speed * (float)delta);
	}
}
