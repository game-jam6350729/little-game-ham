extends Area2D
class_name Interact

@onready var ui_sprite : Sprite2D = $UISprite
@onready var player_parent : Player =  get_parent()
@export var effected_nodes: Array[ResourceNodeType]

var resource_node : ResourceNode = null
var callable 
# Called when the node enters the scene tree for the first time.
func _ready():
	ui_sprite.visible = false
	body_entered.connect(_on_body_entered)
	body_exited.connect(_on_body_exited)
	pass # Replace with function body.

# Detect body around the player to allow interaction with it and display ui
func _on_body_entered(body) -> void:
	# Detect resources nodes
	if body is Chest:
		ui_sprite.visible = true
		callable = func() : body.ShopOpen.emit()
	if body is Trough:
		ui_sprite.visible = true
		callable = func() : body.TroughOpen.emit()
	elif body is ResourceNode:
		# check if the node type is handle by the "player"
		for type in effected_nodes:
			if(body.node_type == type and body.is_gatherable):
				ui_sprite.visible = true
				resource_node = body
	
	

# Clear the detection of the body and hide ui
func _on_body_exited(body)-> void:
	if body is ResourceNode or body is Chest:
		ui_sprite.visible = false
		resource_node = null
		callable = null

# On player action interact with the object in the zone
func _unhandled_input(event: InputEvent):
	if resource_node and resource_node.is_gatherable and event.is_action_pressed("ui_action"):
		if resource_node.harvest(1):
			var inventory : = player_parent.find_child("Inventory")
			if(inventory):
				inventory.add_resources( resource_node.resource_item, 1)
			ui_sprite.visible  = resource_node.current_resources > 0
		else :
			ui_sprite.visible = false
	if callable and event.is_action_pressed("ui_action"):
		callable.call()
		
