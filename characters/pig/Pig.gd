extends CharacterBody2D
class_name Pig

signal isDead
@export var speed = 10
@export var is_taking_damage : bool = true

@onready var nav_agent : NavigationAgent2D= $Navigation/NavigationAgent
@onready var timer_random : Timer = $Navigation/TimerRandom

@onready var timer_damage : Timer = $TimerDamage
@onready var timer_poop : Timer = %TimerPoop

@onready var sprite_default : Sprite2D = $Sprite2D
@onready var sprite_dead : Sprite2D = $SpriteDeath

@export var poopy_scene : PackedScene

@export var life: int = 10: 
	set(value):
		life = value
		if life == 0:
			isDead.emit()
			sprite_default.visible = false
			sprite_dead.visible = true
			timer_damage.stop()
			timer_random.stop()
			timer_poop.stop()
			
		
@export var duration_timer_damage: float = 10

var target_trough: Trough = null

@export var hunger: int = 10
func _ready():
	timer_damage.timeout.connect(_take_damage)
	timer_random.start(1)
	timer_poop.timeout.connect(_shit_happen)
	pass
	
func _process(_delta):
	# If the pig is hungry a 
	if hunger == 0 and timer_damage.is_stopped():
		timer_damage.start(duration_timer_damage)
	if hunger > 0:
		timer_damage.stop()
	
func _physics_process(_delta :float) -> void:
	var axis = to_local(nav_agent.get_next_path_position()).normalized()
	var intended_velocity = axis * speed
	nav_agent.set_velocity(intended_velocity)
	
func _on_timer_hunger_timeout() -> void:
	if hunger > 0:
		hunger -= 1;
	else:
		self.find_nearest_trough()


func find_nearest_trough() ->void:
	var nodes = get_tree().get_nodes_in_group("Trough") as Array[Trough]
	if !nodes.is_empty():
		var nearest_trough = nodes[0]
		for trough_loop in nodes:
			if trough_loop.current_resources > 0 and trough_loop.global_position.distance_to(self.global_position) < nearest_trough.global_position.distance_to(self.global_position):
				nearest_trough = trough_loop
		target_trough = nearest_trough


func _on_trough_agent_navigation_finished():
	if target_trough:
		if target_trough.current_resources > 0:
			target_trough.harvest(1)
			self.hunger = 10
			target_trough = null
			velocity = Vector2.ZERO
			_on_timer_random_timeout()
			timer_damage.stop()
		else:
			find_nearest_trough()
			
	else:
		timer_random.start(randf()* (randi() %20) )
		
func _on_timer_trough_timeout():
	if target_trough:
		nav_agent.target_position = target_trough.global_position
		nav_agent.path_desired_distance = 24
		if self.global_position.y <= target_trough.global_position.y + 8 && self.global_position.y >= target_trough.global_position.y - 8:
			nav_agent.path_desired_distance = 26


func _on_agent_velocity_computed(safe_velocity):
	velocity = safe_velocity # Replace with function body.
	move_and_slide()


func _on_timer_random_timeout():
	var r = randf_range(0,10_000)
	var theta = randf_range(0,2*PI)
	nav_agent.target_position =  Vector2(self.global_position.x + sqrt(r) * cos(theta),self.global_position.y + sqrt(r) * sin(theta))


func _take_damage():
	if(is_taking_damage):
		life -= 1 
		$AnimationPlayer.play("damage")

func _shit_happen():
	var shitScene = poopy_scene.instantiate() as Poop
	shitScene.global_position = global_position
	get_parent().add_child(shitScene)
