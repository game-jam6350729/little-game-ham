extends Resource
class_name ResourceItem 

@export var name := ""
@export_multiline var description := ""
@export var max_stack_count := 1
@export var value:= 1 
@export var texture : Texture
@export var texture_store : Texture
@export var texture_store_border : Texture
@export var texture_store_border_highlighted : Texture
@export var default_properties : Dictionary
