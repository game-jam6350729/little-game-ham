@tool
extends AudibleButton
class_name StoreItemDetail

@export var resource : ResourceItem

@onready var texture_item : TextureRect = %TextureItem
@onready var label_title  : Label = %LabelTitle
#@onready var label_desc   : Label = %LabelDesc
@onready var label_price  : Label = %LabelPrice


func _ready():
	define_resource(resource)

func define_resource(presource : ResourceItem):
	if presource:
		resource = presource
		texture_item.set_texture(resource.texture)
		label_title.text = tr(resource.name)
		#label_desc.text = p_resource.description
		label_price.text = str(resource.value)

func update_price(value : int) -> void:
	label_price.text = str(value)
