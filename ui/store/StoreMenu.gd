@tool
extends ColorRect
class_name StoreMenu


signal StoreClose
@export var item_display_template : PackedScene
@export var resource_value : Dictionary = {}
@export var timer_duration : int = 60
@export var factor_price : float = 1.1
@export var gold : ResourceItem
@export var sfx_buy : AudioStreamPlayer
@export var sfx_sell : AudioStreamPlayer


@onready var animator: AnimationPlayer = $AnimationPlayer
@onready var sell_container : VBoxContainer =  %SellContainer
@onready var buy_container : VBoxContainer =  %BuyContainer
@onready var timer : Timer = %Timer

var player_inventory : Inventory
var _is_active : bool = false
var count_timer : int = 0
var displays : Array[StoreItemDetail] =[]

# Called when the node enters the scene tree for the first time.
func _ready():
	timer.timeout.connect(_on_timer_end)
	var player : Player = get_tree().get_first_node_in_group("Player")
	if player:
		player_inventory = player.find_child("Inventory") as Inventory


func _unhandled_input(event):
	if event.is_action_pressed("ingame_cancel"):
		_is_active = false
		close()

func open() -> void:
	animator.play("Pause")
	get_tree().paused = true
	_is_active = true

	if player_inventory:
		for resource in player_inventory.resources:
			if resource != gold:
				_update_sell(resource)

	for child in buy_container.get_children():
		child.connect("pressed",func(): _on_buy(child))

	##########

	for child in buy_container.get_children():
		if child.resource.name in resource_value:
			child.update_price(resource_value[child.resource.name])

	for child in sell_container.get_children():
		if child.resource.name in resource_value:
			child.update_price(resource_value[child.resource.name])

	_update_focus()


func close() -> void:
	animator.play("Unpause")
	for child in sell_container.get_children():
		sell_container.remove_child(child)
		child.queue_free()
	displays.clear()
	StoreClose.emit()

func _on_buy(node : StoreItemDetail):
	if not player_inventory:
		return
	if _is_active and player_inventory.resources[gold] >= node.resource.value :
		player_inventory.remove_resources(gold, node.resource.value)
		player_inventory.add_resources(node.resource, 1)
		sfx_buy.play()
		_update_sell(node.resource)
		_update_focus(node)

func _on_sell(node : StoreItemDetail):
	if not player_inventory:
		return
	if _is_active and player_inventory.resources[node.resource] > 0:
		player_inventory.remove_resources(node.resource, 1)
		player_inventory.add_resources(gold, node.resource.value)
		sfx_sell.play()
		_update_focus(node)

func _update_sell(type: ResourceItem) -> void:
	if type != gold:
		var current_display: StoreItemDetail
		for display in displays:
			if display.resource == type:
				current_display = display
		#else create new
		if !current_display and type.name in resource_value:
			current_display = item_display_template.instantiate() as StoreItemDetail
			sell_container.add_child(current_display)
			type.value = resource_value[type.name]
			current_display.define_resource(type)
			displays.append(current_display)
			current_display.connect("pressed",func(): _on_sell(current_display))

func _update_focus(prev_focus : Control = null):
	var state = {
		next_best = (prev_focus == null),
		focus_set = false,
		last = null,
	}

	var enable = func(child : Control):
		child.disabled = false
		child.focus_mode = FOCUS_ALL
		if state.focus_set:
			pass
		elif child == prev_focus or state.next_best:
			child.grab_focus()
			state.focus_set = true
		elif not state.focus_set:
			state.last = child

	var disable = func(child : Control):
		child.disabled = true
		child.focus_mode = FOCUS_NONE
		if state.focus_set:
			pass
		elif child == prev_focus:
			state.next_best = true

	for child in sell_container.get_children():
		if player_inventory and child.resource in player_inventory.resources and player_inventory.resources[child.resource] > 0:
			enable.call(child)
		else:
			disable.call(child)

	for child in buy_container.get_children():
		if player_inventory and player_inventory.resources[gold] >= child.resource.value:
			enable.call(child)
		else:
			disable.call(child)

	if not state.focus_set and state.last:
		state.last.grab_focus()

func _on_timer_end():
	count_timer += 1
	var keys = resource_value.keys()
	keys.sort()
	for resource in keys:
		match resource:
			"Fodder":
				resource_value[resource] = floori(((((timer_duration * count_timer) - timer_duration) / 2.0) + timer_duration) /6.0)
			"Manure":
				resource_value[resource] = floori(resource_value["Fodder"]/10 * exp(factor_price))
			"Stone":
				if resource_value[resource] > 1:
					resource_value[resource] -= 1
			"Parts":
				resource_value[resource] = floori((2*(11 + 4*(count_timer +1)))/count_timer + ((-1 + count_timer )*resource_value[resource] )/count_timer)
			_:
				resource_value[resource] = 0
