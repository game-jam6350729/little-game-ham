@tool
extends StoreItemDetail
class_name StoreItemDetailOffer

@export var default_border : Texture
@export var default_border_highlighted : Texture

@onready var border : TextureRect = %Border


func define_resource(presource : ResourceItem):
	if presource:
		resource = presource
		if resource.texture_store_border and resource.texture_store_border_highlighted:
			texture_item.set_texture(resource.texture_store_border)
			border.set_texture(null)
		elif resource.texture_store:
			texture_item.set_texture(resource.texture_store)
			border.set_texture(default_border)
		else:
			texture_item.set_texture(resource.texture)
			border.set_texture(default_border)
		label_title.text = tr(resource.name)
		#label_desc.text = p_resource.description
		label_price.text = str(resource.value)


func _on_focus_entered():
	if resource.texture_store_border and resource.texture_store_border_highlighted:
		texture_item.set_texture(resource.texture_store_border_highlighted)
	else:
		border.set_texture(default_border_highlighted)


func _on_focus_exited():
	if resource.texture_store_border and resource.texture_store_border_highlighted:
		texture_item.set_texture(resource.texture_store_border)
	else:
		border.set_texture(default_border)
