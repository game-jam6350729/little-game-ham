extends AudibleButton
class_name QuitButton



func _pressed() -> void:
	get_tree().quit()
