extends Button
class_name AudibleButton

@export var focused_sfx : AudioStreamPlayer
@export var pressed_sfx : AudioStreamPlayer


func _on_focus_entered() -> void:
	if focused_sfx:
		focused_sfx.play()

func _pressed() -> void:
	if pressed_sfx:
		pressed_sfx.play()
