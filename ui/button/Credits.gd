extends AudibleButton
class_name CreditsButton

# Called when the node enters the scene tree for the first time.
func _ready():
	grab_focus()
	pass # Replace with function body.



func _pressed() -> void:
	SceneTransition.change_scene_to_file("res://levels/Credits.tscn")
	
