extends ColorRect
class_name TroughMenu


signal TroughClose

@onready var quantity_label : Label  = %QuantityLabel
@onready var yes_button : Button = %YesButton
@onready var no_button : Button = %NoButton
@onready var animator : AnimationPlayer = %AnimationPlayer

var trough : Trough
# Called when the node enters the scene tree for the first time.
func _ready():
	yes_button.pressed.connect(validate)
	no_button.pressed.connect(close)


func validate():
	refill()
	close()

func refill(execute := true) -> int:
	if not trough:
		return 0
	var player : Player = get_tree().get_first_node_in_group("Player")
	if not player:
		return 0
	var player_inventory = player.find_child("Inventory") as Inventory
	if player_inventory.resources.has(trough.resource_item):
		var amount = min(
			player_inventory.resources[trough.resource_item],
			trough.max_element - trough.current_resources
		)
		if execute:
			player_inventory.remove_resources(trough.resource_item, amount)
			print("Refilling ", amount, " ", trough.resource_item.name)
			trough.refill(amount)
		return amount
	return 0

func open(ptrough : Trough):
	animator.play("Pause")
	trough = ptrough
	if refill(false) > 0:
		yes_button.disabled = false
		yes_button.grab_focus()
	else:
		yes_button.disabled = true
		no_button.grab_focus()
	quantity_label.text = str( "%0*d/%0*d" % [2, trough.current_resources,2, trough.max_element])

func _unhandled_input(event):
	if event.is_action_pressed("ingame_cancel") and trough:
		close()

func close() -> void:
	animator.play("Unpause")
	#trough = null
	yes_button.release_focus()
	no_button.release_focus()
	TroughClose.emit()
