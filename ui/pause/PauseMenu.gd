@tool
extends ColorRect
class_name PauseMenu

signal PauseClose
@onready var animator: AnimationPlayer = $AnimationPlayer
@onready var play_button : Button = %ResumeButton
@onready var menu_button : Button = %MenuButton
@onready var quit_button : Button = %QuitButton
@onready var timer: Timer = $Timer

var _is_active : bool = false
func _ready():
	timer.timeout.connect(_timer_end)
	
func _process(_delta):
	if _is_active and play_button and not play_button.pressed.is_connected(unpause):
		play_button.grab_focus()
		play_button.pressed.connect(unpause)
		menu_button.pressed.connect(go_to_menu)
		quit_button.pressed.connect(get_tree().quit)
	elif not _is_active and play_button and play_button.pressed.is_connected(unpause):
		play_button.pressed.disconnect(unpause)
		menu_button.pressed.disconnect(go_to_menu)
		quit_button.pressed.disconnect(get_tree().quit)
		play_button.release_focus()
		menu_button.release_focus()
		quit_button.release_focus()

func unpause() -> void:
	_is_active = false
	timer.start()

func pause() -> void:
	animator.play("Pause")
	_is_active = true

func go_to_menu() -> void:
	get_tree().change_scene_to_file("res://levels/menu.tscn")
	get_tree().paused = false

func _timer_end() -> void:
	animator.play("Unpause")
	PauseClose.emit()
	
func _unhandled_input(event):
	if event.is_action_pressed("ingame_cancel") and _is_active:
		_is_active = false
		unpause()
	elif event.is_action_pressed("ingame_pause") and not _is_active:
		_is_active = true
		pause()
