extends GridContainer

class_name ResourceItemDisplay

@onready var texture_rect :  TextureRect = $TextureRect
@onready var label :  Label = $Label

var resource_type : ResourceItem:
	set(new_type):
		resource_type = new_type
		texture_rect.texture = new_type.texture
	

func update_count(count : int) -> void:
	label.text = str( "%0*d" % [3, count] )
