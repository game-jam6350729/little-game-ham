extends MarginContainer
class_name ResourceDisplay


@export var item_display_template : PackedScene

@onready var display_container : GridContainer = $GridContainer

var displays : Array[ResourceItemDisplay] =[]
var player_inventory : Inventory
func _ready():
	var player : Player = get_tree().get_first_node_in_group("Player")
	player_inventory = player.find_child("Inventory") as Inventory
	player_inventory.connect("resource_count_changed", _on_player_inventory_count_changed)

func _on_player_inventory_count_changed(type : ResourceItem , amount: int) -> void:
	# Find existing item display for type
	var current_display: ResourceItemDisplay
	for display in displays:
		if display.resource_type == type:
			current_display = display
	#else create new
	if !current_display:
		current_display = item_display_template.instantiate() as ResourceItemDisplay
		display_container.add_child(current_display)
		current_display.resource_type = type
		displays.append(current_display)
	current_display.update_count(amount)
