extends Node

@onready var play_button : Button = %PlayButton

# Called when the node enters the scene tree for the first time.
func _ready():
	play_button.grab_focus()
	play_button.pressed.connect(func(): SceneTransition.change_scene_to_file("res://levels/Mockup.tscn"))
	pass # Replace with function body.


