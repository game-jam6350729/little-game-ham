extends Node

@onready var button : Button = $Button
# Called when the node enters the scene tree for the first time.
func _ready():
	button.grab_focus()
	button.pressed.connect(func(): get_tree().change_scene_to_file("res://levels/menu.tscn"))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
