extends Node

@onready var animator = $AnimationPlayer
# Called when the node enters the scene tree for the first time.
func _ready():
	animator.animation_finished.connect(func(_string): SceneTransition.change_scene_to_file("res://levels/menu.tscn"))
	pass # Replace with function body.

